﻿using Grpc.Net.Client;
using Server.Protos;
using System;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new SayName.SayNameClient(channel);

            string name = Console.ReadLine();

            var input = new Request { Name = name };

            client.SayClientName(input);

            Console.ReadLine();
        }
    }
}
