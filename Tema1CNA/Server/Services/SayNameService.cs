﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Server.Protos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class SayNameService : SayName.SayNameBase
    {
        private object _logger;

        public SayNameService(ILogger<SayNameService> logger)
        {
            _logger = logger;
        }

        public override Task<Reply> SayClientName(Request request, ServerCallContext context)
        {
            Reply output = new Reply();

            Console.WriteLine($"Hello {request.Name}!");
            return Task.FromResult(output);
        }
    }
}
